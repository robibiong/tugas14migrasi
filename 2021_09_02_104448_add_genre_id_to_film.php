<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGenreIdToFilm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('film', function (Blueprint $table) {
            //tambahin kolom
            $table->unsignedBigInteger('genre_id');
            //tambahin forein key
            $table->foreign('genre_id')->references('id')->on('genre');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('film', function (Blueprint $table) {
            //menghapus foreignkey genre id pada tabel film
            $table->dropforeign(['genre_id']);
            //menghapus kolom yang di add
            $table->dropcolumn('genre_id');
        });
    }
}

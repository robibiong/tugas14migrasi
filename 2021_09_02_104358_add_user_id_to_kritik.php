<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserIdToKritik extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kritik', function (Blueprint $table) {
            //tambahin kolom
            $table->unsignedBigInteger('user_id');
            //tambahin forein key
            $table->foreign('user_id')->references('id')->on('kritik');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kritik', function (Blueprint $table) {
            //menghapus foreignkey user id antara profile dan users
            $table->dropforeign(['user_id']);
            //menghapus kolom yang di add
            $table->dropcolumn('user_id');

        });
    }
}

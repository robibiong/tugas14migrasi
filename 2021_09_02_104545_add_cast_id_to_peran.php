<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCastIdToPeran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('peran', function (Blueprint $table) {
            //tambahin kolom
            $table->unsignedBigInteger('cast_id');
            //tambahin forein key
            $table->foreign('cast_id')->references('id')->on('cast');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('peran', function (Blueprint $table) {
            //menghapus foreignkey 
            $table->dropforeign(['cast_id']);
            //menghapus kolom yang di add
            $table->dropcolumn('cast_id');
        });
    }
}
